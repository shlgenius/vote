<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.sql.*,javax.sql.*,java.io.*"%>

<html>
<head>
<meta charset="utf-8">
<style>
	table.TB { 
		border-spacing: 10px;							
	}
	table.TB td {
		line-height: 2.5em;
		border: 1px solid #000000;		
	}
	table.TB td a {
		display: block;
		width: 100%;
		height: 100%;
	}
	table.TB td a:hover {
		color: #FFFFFF;
		text-decoration: none;
		background-color: #000000;		
	}
	table.TB td a:active {
		color: #FFFFFF;
		text-decoration: none;
		background-color: #000000;
	}	
	a:link { color: #000000; text-decoration: none; }
	a:visited { color: #000000; text-decoration: none; }
	a:hover { color: #000000; text-decoration: underline; font-size: 16px; font-weight: bold;}
	a:active { color: #000000; text-decoration: none; font-weight: bold;}		
</style>
<style>
	.button {
		width: 80px;
		height: 40px;
		padding: 2px 2px 2px 2px; /* 상, 우, 좌, 하 */
		text-align: center;
		border: 1px solid #000000;		
		background-color : #D8D8D8;	
		
		font-size: 15px;
		font-weight: bold;
	}
	input:hover[type="submit"] {
		background: #000000;
		color: #FFFFFF;
	}
</style>
<%!		
	// 애러코드를 잡기 위해 함수 선언
	// 여기서는 각종 테스트를 위해 활용한 이후 애러코드의 대부분을 해결한 이후 replace 방식을 사용
	public static int extractErrorCode (SQLException sqlException) {
		int errorCode = sqlException.getErrorCode();
		SQLException nested = sqlException.getNextException();
		while(errorCode==0&&nested!=null){
			errorCode=nested.getErrorCode();
			nested=nested.getNextException();
		}
		return errorCode;
	}
%>
</head>
<body>
<center>
<br>
<h1>투표시스템</h1>
<table  class="TB">
	<tr>
		<ul>
		<td width=200 height=40 align=center><a href="A01.jsp"><b>후보등록</b></a></td>
		<td width=200 height=40 align=center><a href="B01.jsp"><b>투 표</b></a></td>
		<td width=200 height=40 align=center bgcolor="#D8D8D8"><a href="C01.jsp"><b>개표결과</b></a></td>
		</ul>
	</tr>
</table>
<br>
<br>
<h2>개표결과 페이지</h2>

<%
	// 데이터베이스 연결
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/shl","root","111");
	
	// Statement 선언 및 설정
	Statement stmt1 = conn.createStatement();
	Statement stmt2 = conn.createStatement();
	Statement stmt3 = conn.createStatement();
	Statement stmt4 = conn.createStatement();	

	// ResultSet 선언 및 설정
	ResultSet rset1 = stmt1.executeQuery("select * from hubo_table;");
	ResultSet rset2 = stmt2.executeQuery("select count(*) from hubo_table;");
	ResultSet rset3 = stmt3.executeQuery("select count(*) from tupyo_table;");
	ResultSet rset4 = stmt4.executeQuery("select h.id,h.name,ifnull(t.count,0) as '득표수',round(ifnull(t.count,0)/(select count(*) from tupyo_table)*100,2) as '득표율' from hubo_table h left join (select id,count(*) as count from tupyo_table group by id) t on h.id=t.id;");
	
	// 변수 선언 및 설정 (데이터 유무 확인용; 막대그래프 최대 길이; 막대그래프 최종길이;)
	int totalRecords=0;
	int barWidth=360;
	double resultWidth=0;
	int count=0;
	
	// 데이터 유무 판단을 위한 조건문
	while(rset2.next()){
		totalRecords=rset2.getInt(1);
	}

	// 데이터 유무 판단을 위한 조건문
	while(rset3.next()){
		count=rset3.getInt(1);
	}

	// 데이터가 존재하지 않을 경우 조건문 이하를 실행
	if(count==0){
%>
<!-- 데이터가 존재할 하지 않을 경우 테이블 생성 후 출력문 표시 -->
<table width=680 cellspacing=2 border=2>
	<tr>		
		<td width=300 height=40 align=center>투표가 진행되지 않았습니다.</td>		
	</tr>
</table>
<%
	// 만약 데이터가 존재할 경우 조건문 이하 실행	
	} else if(totalRecords!=0){
%>
<!-- 데이터가 존재할 경우 테이블 생성 후 반복문 실행 -->
<table width=680 cellspacing=2 border=2>
	<tr>
		<td colspan=2 height=40>&emsp;<font><b>후보 별 득표율</b></font></td>
	</tr>
<%
		// ResultSet 이 존재하는 동안 이하 반복문 실행
		while(rset4.next()){

			// 막대그래프 최종길이 설정을 위한 설정
			resultWidth=barWidth*rset4.getDouble(4)/100;
%>	
	<!-- 반복문으로 각 후보별 득표율을 시각화 -->
	<!-- 반복문으로 각 후보별 링크 설정; C02.jsp로 설정; parameter 값 설정; -->
	<!-- 막대그래프는 그림판으로 만든 후 서버에 복사하여 사용 -->
	<tr>
		<td width=180 height=40 align=left>&emsp;<a href="C02.jsp?id=<%=rset4.getInt(1)%>">기호 <%=rset4.getInt(1)%>번 <%=rset4.getString(2)%></a></td>
		<td height=40 align=left><img src="http://192.168.23.18:8082/Vote/bar.png" width="<%=resultWidth%>" height=25 border=0><b> <%=rset4.getInt(3)%>표 (<%=rset4.getDouble(4)%>%)</b></td>				
	</tr>
<%		
		}
%>	
</table>
<%
	// 위 두 조건을 만족하지 않는 경우 이하 조건문 실행
	} else {
%>
<!-- 이 조건에 해당할 경우 테이블 생성 및 출력문 표시 -->
<table width=680 cellspacing=2 border=2>
	<tr>		
		<td width=300 height=40 align=center>등록된 후보가 없습니다.</td>		
	</tr>
</table>
<%		
	}
	// 자원 반환
	rset1.close();
	rset2.close();
	rset3.close();
	rset4.close();		
	stmt4.close();
	stmt3.close();
	stmt2.close();
	stmt1.close();
	conn.close();	
%>

</body>
</html>