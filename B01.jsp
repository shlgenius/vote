<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.sql.*,javax.sql.*,java.io.*"%>
<html>
<head>
<meta charset="utf-8">
<style>
	table.TB { 
		border-spacing: 10px;
	}
	table.TB td {
		line-height: 2.5em;
		border: 1px solid #000000;		
	}
	table.TB td a {
		display: block;
		width: 100%;
		height: 100%;
	}
	table.TB td a:hover {
		background-color: #000000;
	}
	table.TB td a:active {
		background-color: #000000;
	}	
	a:link { color: #000000; text-decoration: none; }
	a:visited { color: #000000; text-decoration: none; }
	a:hover { color: #FFFFFF; background-color: #000000; }
	a:active { color: #FFFFFF; background-color: #000000; }		
</style>
<style>
	.button {
		width: 80px;
		height: 40px;
		padding: 2px 2px 2px 2px; /* 상, 우, 좌, 하 */
		text-align: center;
		border: 1px solid #000000;		
		background-color : #D8D8D8;	
		
		font-size: 15px;
		font-weight: bold;
	}
	input:hover[type="submit"] {
		background: #000000;
		color: #FFFFFF;
	}
</style>
<style>
	.input {
		padding-left: 15px;		
		width: 120px;
		height: 25px;		
	}
</style>
<style>
	.input2 {
		padding-left: 10px;		
		width: 180px;
		height: 25px;		
	}
</style>
</head>
<body>
<center>
<br>
<h1>투표시스템</h1>
<table  class="TB">
	<tr>
		<ul>
		<td width=200 height=40 align=center><a href="A01.jsp"><b>후보등록</b></a></td>
		<td width=200 height=40 align=center bgcolor="#D8D8D8"><a href="B01.jsp"><b>투 표</b></a></td>
		<td width=200 height=40 align=center><a href="C01_ani.jsp"><b>개표결과</b></a></td>
		</ul>
	</tr>
</table>
<br>
<br>
<h2>투표 페이지</h2>

<%
	// 데이터베이스 연결
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/shl","root","111");
	
	// Statement 선언 및 설정
	Statement stmt1 = conn.createStatement();
	Statement stmt2 = conn.createStatement();
	Statement stmt3 = conn.createStatement();

	// ResultSet 선언 및 설정
	ResultSet rset1 = stmt1.executeQuery("select * from hubo_table;");
	ResultSet rset2 = stmt2.executeQuery("select count(*) from hubo_table;");
	ResultSet rset3 = stmt3.executeQuery("select id from hubo_table;");

	// 변수 선언 및 설정 (데이터 유무 확인용; 후보번호 자동생성용)
	int totalRecords=0;
	int candidateNumber=1;
	
	// 데이터 유무 판단을 위한 조건문
	while(rset2.next()){
		totalRecords=rset2.getInt(1);
	}
	
	// 후보 번호 설정을 위한 조건문
	while(rset3.next()){
		if(candidateNumber==rset3.getInt(1)){
			candidateNumber=candidateNumber+1;
		} else {
			break;
		}
	}
	
	// 데이터가 존재할 경우 조건문 이하를 실행
	if(totalRecords!=0){
%>
	<!-- 테이블 생성 -->
	<!-- 투표하기 버튼을 위한 form-action 설정 : 실행시 B02.jsp 실행 지정 -->
	<!-- select-option 박스 생성 -->
	<!-- select-option 박스 닫은 후에 투표하기 버튼 생성; 스타일 지정; -->
	<table width=680 cellspacing=2 border=2>
	<tr>
		<td width=300 height=40 align=center><font size=3><b>후보 선택</b></font></td>
		<td width=300 height=40 align=center><font size=3><b>연령대 선택</b></font></td>
		<td align=center><font size=3><b>투표</b></font></td>
	</tr>
	<tr>
		<form method=post action=B02.jsp>
		<td width=300 height=40>&emsp;<select name=id class="input2">
		<option>후보 선택</option>
<%
		// 후보선택 option 지정을 위한 반복문 설정
		// ResultSet 이 존재하는 동안 이하 반복문 실행
		while(rset1.next()){	
%>	
			<!-- 후보 select박스의 option 박스에 ResultSet 의 결과물을 배치 -->
			<option type=number value=<%=rset1.getInt(1)%>>기호<%=rset1.getInt(1)%>번 &emsp;<%=rset1.getString(2)%>&emsp;</option>
<%		
		}
%>
		</select></td>
		<td width=300 height=40>&emsp;<select name=age class="input2">
		<option>연령대 선택</option>
<%
		// 연령대 option 지정을 위한 반복문 설정
		for(int age=10; age<=90; age=age+10){	
%>		
			<!-- 연령대 select박스의 option 박스에 ResultSet 의 결과물을 배치 -->
			<option type=number value=<%=age%>><%=age%> 대</option>
<%
	}
%>		
		</select></td>
		<td><input type=submit value=투표하기 class="button"></input></td>
		</form>
	</tr>
</table>

<%		
	} else{
	// 데이터가 존재하지 않을 경우 이하를 실행
%>
	<!-- 데이터가 존재하지 않을 경우 테이블 생성; 출력문 표시  -->	
	<table width=680 cellspacing=2 border=2>
	<tr>	
		<td width=300 height=40 align="center">등록된 후보가 없습니다.</td>
	</tr>
<%		
	}
	// 자원반환
	rset1.close();
	rset2.close();
	rset3.close();
	
	stmt3.close();
	stmt2.close();
	stmt1.close();
	conn.close();	
%>
</body>
</html>
