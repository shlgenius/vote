<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="content-type" content="text/html">
    </head>
    <body>
        <!--  #투표 프로그램 -->
        <h2>투표 프로그램</h2>
        <br>
        <h3> 후보자 CRUD, 연령별 결과분석 가능한 투표 프로그램 구현 </h3>
        <h3> JSP; MySQL; </h3>
        <h3> 그림파일을 활용한 시각화 </h3>
        <h3> CSS 활용하여 투표 결과를 애니메이션으로 시각화 </h3>
    </body>
</html>
