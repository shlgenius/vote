<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.sql.*,javax.sql.*,java.io.*"%>

<html>
<head>
<meta charset="utf-8">
<style>
	table.TB { 
		border-spacing: 10px;							
	}
	table.TB td {
		line-height: 2.5em;
		border: 1px solid #000000;		
	}
	table.TB td a {
		display: block;
		width: 100%;
		height: 100%;
	}
	table.TB td a:hover {
		background-color: #000000;
	}
	table.TB td a:active {
		background-color: #000000;
	}	
	a:link { color: #000000; text-decoration: none; }
	a:visited { color: #000000; text-decoration: none; }
	a:hover { color: #FFFFFF; background-color: #000000; }
	a:active { color: #FFFFFF; background-color: #000000; }		
</style>
<style>
	.button {
		width: 80px;
		height: 40px;
		padding: 2px 2px 2px 2px; /* 상, 우, 좌, 하 */
		text-align: center;
		border: 1px solid #000000;		
		background-color : #D8D8D8;	
		
		font-size: 15px;
		font-weight: bold;
	}
	input:hover[type="submit"] {
		background: #000000;
		color: #FFFFFF;
	}
</style>
<%	

	// 예외 발생시 replace 를 활용하여 B01.jsp 를 실행
	try{		
	// B01.jsp 에서 투표하기 실행시 form-action으로 인하여 B02.jsp 실행
	// parameter name="id", value="id" 를 받는다
	// parameter name="age", value="age" 를 받는다
	// parameter 값을 바탕으로 B02.jsp를 실행
	request.setCharacterEncoding("UTF-8");
	
	int id;
	String ids = request.getParameter("id");
	id = Integer.parseInt(ids);
	
	int age;
	String ages = request.getParameter("age");
	age = Integer.parseInt(ages);	
%>
<%!	
	// 애러코드를 잡기 위해 함수 선언
	// 여기서는 각종 테스트를 위해 활용한 이후 애러코드의 대부분을 해결한 이후 replace 방식을 사용
	public static int extractErrorCode (SQLException sqlException) {
		int errorCode = sqlException.getErrorCode();
		SQLException nested = sqlException.getNextException();
		while(errorCode==0&&nested!=null){
			errorCode=nested.getErrorCode();
			nested=nested.getNextException();
		}
		return errorCode;
	}
%>
</head>
<body>
<center>
<br>
<h1>투표시스템</h1>
<table  class="TB">
	<tr>
		<ul>
		<td width=200 height=40 align=center><a href="A01.jsp"><b>후보등록</b></a></td>
		<td width=200 height=40 align=center bgcolor="#D8D8D8"><a href="B01.jsp"><b>투 표</b></a></td>
		<td width=200 height=40 align=center><a href="C01_ani.jsp"><b>개표결과</b></a></td>
		</ul>
	</tr>
</table>
<br>
<br>
<h2>투표 페이지</h2>

<%	
	// 데이터베이스 연결
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/shl","root","111");
	
	// Statement 선언 및 설정
	Statement stmt1 = conn.createStatement();
	Statement stmt2 = conn.createStatement();
	Statement stmt3 = conn.createStatement();
	Statement stmt4 = conn.createStatement();
	
	// Statement 실행하여 데이터베이스 변경(입력)
	stmt4.execute("insert into tupyo_table (id,age) values ("+id+","+age+");");
	
	// ResultSet 선언 및 설정
	ResultSet rset1 = stmt1.executeQuery("select * from hubo_table where id="+id+";");
	
	// 변수 선언 및 설정 (결과 출력 위한 아이디; 이름;)
	int resultID=0;
	String resultName="";
	
	// ResultSet 이 존재하는 동안 이하 반복문 실행
	while(rset1.next()){
		
		// 선언한 변수에 ResultSet 의 값을 저장;
		resultID=rset1.getInt(1);
		resultName=rset1.getString(2);
	}
	
	// 자원 반환
	stmt4.close();
	stmt3.close();
	stmt2.close();
	stmt1.close();	
	rset1.close();
	conn.close();
%>

<!-- 변수값을 사용하여 출력문 표시 -->
<h3>투표 결과 : 기호 <%=resultID%>번&emsp;<%=resultName%>에게 투표하였습니다.</h3>
</center>
</body>
<%
	} catch(Exception e){		
		// 예외 발생시 replace 를 활용하여 B01.jsp 를 실행
%>
		<script type="text/javascript">window.location.replace("B01.jsp");</script>	
<%					
	}
%>
</html>
