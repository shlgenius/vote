<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.sql.*,javax.sql.*,java.io.*"%>

<html>
<head>
<meta charset="utf-8">
<style>
	table.TB { 
		border-spacing: 10px;							
	}
	table.TB td {
		line-height: 2.5em;
		border: 1px solid #000000;		
	}
	table.TB td a {
		display: block;
		width: 100%;
		height: 100%;
	}
	table.TB td a:hover {
		color: #FFFFFF;
		text-decoration: none;
		background-color: #000000;		
	}
	table.TB td a:active {
		color: #FFFFFF;
		text-decoration: none;
		background-color: #000000;
	}	
	a:link { color: #000000; text-decoration: none; }
	a:visited { color: #000000; text-decoration: none; }
	a:hover { color: #000000; text-decoration: underline; font-weight: bold;}
	a:active { color: #000000; text-decoration: none; font-weight: bold;}		
</style>
<style>
	.button {
		width: 80px;
		height: 40px;
		padding: 2px 2px 2px 2px; /* 상, 우, 좌, 하 */
		text-align: center;
		border: 1px solid #000000;		
		background-color : #D8D8D8;	
		
		font-size: 15px;
		font-weight: bold;
	}
	input:hover[type="submit"] {
		background: #000000;
		color: #FFFFFF;
	}
</style>
<style>
		.h_graph .g_bar{display:inline-block;position:relative;height:20px;border:1px solid #ccc;border-left:0;background:#e9e9e9;animation: stack 4s 1;}
        .h_graph .g_bar span{position:absolute;top:0;right:-50px;width:40px;color:#767676;line-height:20px;animation: stack 4s 1;}	
</style>
<style>
		body { <!-- padding: 50px;-->}
		.graph { height: 30px; margin: 0 0 0 0 30px; background:#ffffff; }
		.graph span:nth-child(1) { 
			display: inline-block; 
			padding: 0 ; 
			margin: 0 0 0 0; 
			height: 30px; 
			line-height: 30px; 
			text-align: right; 
			box-sizing: border-box; 
			color: white; 
			animation: stack 2s 1;
		}
		.graph.stack1 span { background:violet; animation:stack1 3s 1;}
		.graph.stack2 span { background:#BDBDBD; animation:stack2 2s 1;}
		.graph.stack3 span { background:orange; animation:stack3 3s 1;}
	
		@keyframes stack1{
			0% { width: 0; color: rgba(255,255,255,0); }
			40% { color: rgba(255,255,255,1);}
			100% { width: 75%; }
		}
		@keyframes stack2{
			0% { width: 0; color: rgba(255,255,255,0); }
			40% { color: rgba(255,255,255,1);}			
		}
		@keyframes stack3{
			0% { width: 0; color: rgba(255,255,255,0); }
			40% { color: rgba(255,255,255,1);}
			100% { width: 25%; }
		}
	</style>
<%!		
	// 오류 코드 획득을 위한 함수 선언
	public static int extractErrorCode (SQLException sqlException) {
		int errorCode = sqlException.getErrorCode();
		SQLException nested = sqlException.getNextException();
		while(errorCode==0&&nested!=null){
			errorCode=nested.getErrorCode();
			nested=nested.getNextException();
		}
		return errorCode;
	}
%>
</head>
<body>
<center>
<br>
<h1>투표시스템</h1>
<table  class="TB">
	<tr>
		<ul>
		<td width=200 height=40 align=center><a href="A01.jsp"><b>후보등록</b></a></td>
		<td width=200 height=40 align=center><a href="B01.jsp"><b>투 표</b></a></td>
		<td width=200 height=40 align=center bgcolor="#D8D8D8"><a href="C01_ani.jsp"><b>개표결과</b></a></td>
		</ul>
	</tr>
</table>
<br>
<br>
<h2>개표결과 페이지</h2>

<%
	
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/shl","root","111");
	
	Statement stmt1 = conn.createStatement();
	Statement stmt2 = conn.createStatement();
	Statement stmt3 = conn.createStatement();
	Statement stmt4 = conn.createStatement();	

	ResultSet rset1 = stmt1.executeQuery("select * from hubo_table;");
	ResultSet rset2 = stmt2.executeQuery("select count(*) from hubo_table;");
	ResultSet rset3 = stmt3.executeQuery("select count(*) from tupyo_table;");
	ResultSet rset4 = stmt4.executeQuery("select h.id,h.name,ifnull(t.count,0) as '득표수',round(ifnull(t.count,0)/(select count(*) from tupyo_table)*100,2) as '득표율' from hubo_table h left join (select id,count(*) as count from tupyo_table group by id) t on h.id=t.id;");
	
	int totalRecords=0;
	int barWidth=360;
	double resultWidth=0;
	int count=0;
	
	while(rset2.next()){
		totalRecords=rset2.getInt(1);
	}

	while(rset3.next()){
		count=rset3.getInt(1);
	}
		
	if(count==0){
%>
<table width=680 cellspacing=2 border=2>
	<tr>		
		<td width=300 height=40 align=center>투표가 진행되지 않았습니다.</td>		
	</tr>
</table>
<%
		
	} else if(totalRecords!=0){
%>
<table width=680 cellspacing=2 border=2>
	<tr>
		<td colspan=2 height=40><b>&emsp;후보 별 득표율</b></td>
	</tr>
<%
		while(rset4.next()){			
			resultWidth=barWidth*rset4.getDouble(4)/100;			
%>	
	<tr>
		<td width=170 height=40 align=left>
			&emsp;<a href="C02_ani.jsp?id=<%=rset4.getInt(1)%>">기호 <%=rset4.getInt(1)%>번 <%=rset4.getString(2)%></a>
		</td>		
		<td height=40>
			<div class="graph stack2"><span style="width:<%=resultWidth%>;" height=25 border=0></span> 
			<span style="background:#ffffff;font-weight:bold;"><%=rset4.getInt(3)%>표 (<%=rset4.getDouble(4)%>%)</span></div>            	
		</td>				
	</tr>
<%		
		}
%>	
</table>
<%
	} else {
%>
<table width=680 cellspacing=2 border=2>
	<tr>		
		<td width=300 height=40 align=center>등록된 후보가 없습니다.</td>		
	</tr>
</table>
<%		
	}
	rset1.close();
	rset2.close();
	rset4.close();
		
	stmt4.close();
	stmt3.close();
	stmt2.close();
	stmt1.close();
	conn.close();	
%>

</body>
</html>