<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.sql.*,javax.sql.*,java.io.*"%>

<html>
<head>
<meta charset="utf-8">
<style>
	table.TB { 
		border-spacing: 10px;							
	}
	table.TB td {
		line-height: 2.5em;
		border: 1px solid #000000;		
	}
	table.TB td a {
		display: block;
		width: 100%;
		height: 100%;
	}
	table.TB td a:hover {
		color: #FFFFFF;
		text-decoration: none;
		background-color: #000000;		
	}
	table.TB td a:active {
		color: #FFFFFF;
		text-decoration: none;
		background-color: #000000;
	}	
	a:link { color: #000000; text-decoration: none; }
	a:visited { color: #000000; text-decoration: none; }
	a:hover { color: #000000; text-decoration: underline; font-size: 16px; font-weight: bold;}
	a:active { color: #000000; text-decoration: none; font-weight: bold;}		
</style>
<style>
	.button {
		width: 80px;
		height: 40px;
		padding: 2px 2px 2px 2px; /* 상, 우, 좌, 하 */
		text-align: center;
		border: 1px solid #000000;		
		background-color : #D8D8D8;	
		
		font-size: 15px;
		font-weight: bold;
	}
	input:hover[type="submit"] {
		background: #000000;
		color: #FFFFFF;
	}
</style>
<%	
	// C01.jsp 에서 후보별 득표율 실행시 form-action으로 인하여 C02.jsp 실행
	// parameter name="id", value="id" 를 받는다	
	// parameter 값을 바탕으로 C02.jsp를 실행
	request.setCharacterEncoding("UTF-8");
	
	int id;
	String ids = request.getParameter("id");
	id = Integer.parseInt(ids);
%>

<%!		
	// 애러코드를 잡기 위해 함수 선언
	// 여기서는 각종 테스트를 위해 활용한 이후 애러코드의 대부분을 해결한 이후 replace 방식을 사용
	public static int extractErrorCode (SQLException sqlException) {
		int errorCode = sqlException.getErrorCode();
		SQLException nested = sqlException.getNextException();
		while(errorCode==0&&nested!=null){
			errorCode=nested.getErrorCode();
			nested=nested.getNextException();
		}
		return errorCode;
	}
%>
</head>
<body>
<center>
<br>
<h1>투표시스템</h1>
<table  class="TB">
	<tr>
		<ul>
		<td width=200 height=40 align=center><a href="A01.jsp"><b>후보등록</b></a></td>
		<td width=200 height=40 align=center><a href="B01.jsp"><b>투 표</b></a></td>
		<td width=200 height=40 align=center bgcolor="#D8D8D8"><a href="C01.jsp"><b>개표결과</b></a></td>
		</ul>
	</tr>
</table>
<br>
<br>
<h2>개표결과 페이지</h2>

<%
	// 데이터베이스 연결
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/shl","root","111");
	
	// Statement 선언 및 설정
	Statement stmt1 = conn.createStatement();
	Statement stmt2 = conn.createStatement();
	Statement stmt3 = conn.createStatement();
	Statement stmt4 = conn.createStatement();	
	
	// ResultSet 선언 및 설정
	ResultSet rset1 = stmt1.executeQuery("select * from hubo_table where id="+id+";");
	ResultSet rset2 = stmt2.executeQuery("select count(*) from tupyo_table where id="+id+";");	
	ResultSet rset4 = stmt4.executeQuery("select t.age,count(t.id),round(count(t.id)/(select count(*) from tupyo_table where id="+id+")*100,2) from tupyo_table t where t.id="+id+" group by t.age;");
	
	// 변수 선언 및 설정 (데이터 유무 확인용; 막대그래프 최대 길이; 막대그래프 최종길이;)
	int totalRecords=0;
	int barWidth=360;
	double resultWidth=0;
	int count=0;	
	
	// 데이터 유무 판단을 위한 반복문
	while(rset2.next()){
		count=rset2.getInt(1);
	} 
	
	// 데이터가 존재하는 경우 조건문 이하 실행
	if(count!=0){
%>
<!-- 데이터가 존재할 경우 테이블 생성 후 반복문 실행 -->
<table width=680 cellspacing=2 border=2>
<%
		// ResultSet 이 존재하는 동안 이하 반복문 실행
		// 후보테이블에서 아이디가 일치하는 후보자 조회
		while(rset1.next()){
%>
	<tr>
			<td colspan=2 height=40 align=left>&emsp;<b>기호 <%=rset1.getInt(1)%>번 <%=rset1.getString(2)%></b> 후보 득표성향 분석</td>
	</tr>	
<%
		}

		// ResultSet 이 존재하는 동안 이하 반복문 실행
		// 해당 후보자의 연령별 득표성향을 조회
		while(rset4.next()){

			// 막대그래프 최종길이 설정을 위한 설정
			resultWidth=barWidth*rset4.getDouble(3)/100;
%>	
	<!-- 반복문으로 후보의 연령대별 득표성향 시각화 -->	
	<!-- 막대그래프는 그림판으로 만든 후 서버에 복사하여 사용 -->
	<tr>
		<td width=180 height=40 align=center><%=rset4.getInt(1)%> 대</td>
		<td height=40>
			<img src="http://192.168.23.18:8082/Vote/bar.png" width="<%=resultWidth%>" height=25 border=0> 
			<%=rset4.getInt(2)%>표 (<%=rset4.getDouble(3)%>%)</td>				
	</tr>
<%		
		}
%>	
</table>
<%
	} else if (count==0){
	// 데이터가 존재하지 않는 경우 조건문 이하 실행
%>
<!-- 데이터가 존재할 하지 않을 경우 테이블 생성 후 출력문 표시 -->
<table width=680 cellspacing=2 border=2>
	<tr>		
		<td width=300 height=40 align=center>한표도 받지 못했습니다.</td>		
	</tr>
</table>
<%		
	} 
	//자원 반환
	rset1.close();
	rset2.close();
	rset4.close();
		
	stmt4.close();
	stmt3.close();
	stmt2.close();
	stmt1.close();
	conn.close();	
%>

</body>
</html>