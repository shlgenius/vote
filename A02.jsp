<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.sql.*,javax.sql.*,java.io.*"%>

<html>
<head>
<meta charset="utf-8">
<style>
	table.TB { 
		border-spacing: 10px;							
	}
	table.TB td {
		line-height: 2.5em;
		border: 1px solid #000000;		
	}
	table.TB td a {
		display: block;
		width: 100%;
		height: 100%;
	}
	table.TB td a:hover {
		background-color: #000000;
	}
	table.TB td a:active {
		background-color: #000000;
	}	
	a:link { color: #000000; text-decoration: none; }
	a:visited { color: #000000; text-decoration: none; }
	a:hover { color: #FFFFFF; background-color: #000000; }
	a:active { color: #FFFFFF; background-color: #000000; }		
</style>
<style>
	.button {
		width: 80px;
		height: 40px;
		padding: 2px 2px 2px 2px; /* 상, 우, 좌, 하 */
		text-align: center;
		border: 1px solid #000000;		
		background-color : #D8D8D8;	
		
		font-size: 15px;
		font-weight: bold;
	}
	input:hover[type="submit"] {
		background: #000000;
		color: #FFFFFF;
	}
</style>
<style>
	.input {
		padding-left: 15px;
		width: 120px;
		height: 25px;		
	}
</style>
<%	
	// A01.jsp 에서 삭제 실행시 form-action으로 인하여 A02.jsp 실행
	// parameter name="id", value="id" 를 받는다
	// parameter 값을 바탕으로 A02.jsp를 실행
	request.setCharacterEncoding("UTF-8");
	
	int id;
	String ids = request.getParameter("id");
	id = Integer.parseInt(ids);
	
	String name = request.getParameter("name");
%>

<%!		
	// 애러코드를 잡기 위해 함수 선언
	// 여기서는 각종 테스트를 위해 활용한 이후 애러코드의 대부분을 해결한 이후 replace 방식을 사용
	public static int extractErrorCode (SQLException sqlException) {
		int errorCode = sqlException.getErrorCode();
		SQLException nested = sqlException.getNextException();
		while(errorCode==0&&nested!=null){
			errorCode=nested.getErrorCode();
			nested=nested.getNextException();
		}
		return errorCode;
	}
%>
</head>
<body>
<center>
<br>
<h1>투표시스템</h1>
<table  class="TB">
	<tr>
		<ul>
		<td width=200 height=40 align=center bgcolor="#D8D8D8"><a href="A01.jsp"><b>후보등록</b></a></td>
		<td width=200 height=40 align=center><a href="B01.jsp"><b>투 표</b></a></td>
		<td width=200 height=40 align=center><a href="C01_ani.jsp"><b>개표결과</b></a></td>
		</ul>
	</tr>
</table>
<br>
<br>
<h2>후보등록 페이지</h2>

<%
	// 데이터베이스 연결
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/shl","root","111");
	
	// Statement 선언 및 설정
	Statement stmt1 = conn.createStatement();
	Statement stmt2 = conn.createStatement();
	Statement stmt3 = conn.createStatement();
	Statement stmt4 = conn.createStatement();
	Statement stmt5 = conn.createStatement();
	
	// Statement 실행하여 데이터베이스 변경(삭제)
	stmt4.execute("delete from hubo_table where id="+id+";");
	stmt5.execute("delete from tupyo_table where id="+id+";");
	
	// ResultSet 선언 및 설정
	ResultSet rset1 = stmt1.executeQuery("select * from hubo_table;");
	ResultSet rset2 = stmt2.executeQuery("select count(*) from hubo_table;");
	ResultSet rset3 = stmt3.executeQuery("select id from hubo_table;");

	// 변수 선언 및 설정 (데이터 유무 확인용; 후보번호 자동생성용)
	int totalRecords=0;
	int candidateNumber=1;
	
	// 데이터 유무 판단을 위한 조건문
	while(rset2.next()){
		totalRecords=rset2.getInt(1);
	}
	
	// 후보 번호 설정을 위한 조건문
	while(rset3.next()){
		if(candidateNumber==rset3.getInt(1)){
			candidateNumber=candidateNumber+1;
		} else {
			break;
		}
	}	
	
	// 데이터가 존재할 경우 조건문 이하를 실행
	if(totalRecords!=0){
%>
<!-- 테이블 생성 -->
<table width=680 cellspacing=2 border=2>
	<tr>
		<td width=300 height=40 align=center><font size=3><b>기호번호</b></font></td>
		<td width=300 height=40 align=center><font size=3><b>후보명</b></font></td>
		<td align=center><font size=3><b>추가/삭제</b></font></td>
	</tr>
<%
		// ResultSet 이 존재하는 동안 이하 반복문 실행
		while(rset1.next()){
%>
	<!-- 테이블에 ResultSet을 출력 -->
	<!-- 삭제 버튼 생성 및 submit 실행시 지정한 form 으로 이동 -->
	<!-- 스타일의 button클래스 사용 -->
	<tr>
		<form method=post action="A02.jsp?id=<%=rset1.getInt(1)%>">	
		<td width=300 height=40>&emsp;기호번호 : &emsp;<%=rset1.getInt(1)%></td>
		<td width=300 height=40>&emsp;후보명 : &emsp;<%=rset1.getString(2)%></td>
		<td><input type=submit value=삭제 class="button"></input></td>
		</form>
	</tr>

<%		
		}
%>
	<!-- 반복문 완료 후 이하 실행 -->
	<!-- 반복문 완료 추가 명령을 위한 행 생성 -->
	<!-- 추가 버튼 생성 및 submit 실행시 지정한 form 으로 이동 -->
	<tr>
		<form method=post action=A03.jsp>
		<td width=300 height=40>&emsp;기호번호 : <input type=number name=id min=0 class="input" required placeholder=<%=candidateNumber%> value=<%=candidateNumber%> readonly > </input></td>
		<td width=300 height=40>&emsp;후보명 : <input type=text name=name class="input" placeholder="한글만 가능" pattern="^[가-힣]{1,5}$" required ></input></td>
		<td><input type=submit value=추가 class="button"></input></td>
		</form>
	</tr>
</table>
<%
	} else {
	// 데이터가 존재하지 않을 경우 이하를 실행
%>
<!-- 데이터가 존재하지 않을 경우 추가 버튼만 생성 -->
<!-- 추가 버튼 생성 및 submit 실행시 지정한 form 으로 이동 -->
<table width=680 cellspacing=2 border=2>
	<tr>
		<td width=300 height=40 align=center><font size=3><b>기호번호</b></font></td>
		<td width=300 height=40 align=center><font size=3><b>후보명</b></font></td>
		<td align=center><font size=3><b>추가/삭제</b></font></td>
	</tr>
	<tr>
		<form method=post action=A03.jsp>
		<td width=300 height=40>&emsp;기호번호 : <input type=number name=id min=0 class="input" required placeholder=<%=candidateNumber%> value=<%=candidateNumber%> readonly ></input></td>
		<td width=300 height=40>&emsp;후보명 : <input type=text name=name class="input" placeholder="한글만 가능" pattern="^[가-힣]{1,5}$" required ></input></td>
		<td><input type=submit value=추가 class="button"></input></td>
		</form>
	</tr>
</table>
<%	
	}
	// 자원 반환
	rset1.close();
	rset2.close();
	rset3.close();
	
	stmt5.close();
	stmt4.close();
	stmt3.close();
	stmt2.close();
	stmt1.close();
	
	conn.close();
%>

</body>
</html>